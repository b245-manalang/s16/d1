// console.log("Hello World");

// [SECTION] Arithmetic Operators
	// allow us to perform mathematical operations between operands (value on either sides of the operator).
	// it returns a numerical value.

	let x = 102;
	let y = 25;

	let sum = x + y;
	console.log("Result of addition operator: " + sum)
		;

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	// modulo or remainder
	let mod = x % y;
	console.log("Result of modulo operator:" + mod);

	// Assignment Operator
		// assigns the value of the right operand to a "variable".

	// Basic Assignment Operator
	let assignmentNumber = 8;
	console.log(assignmentNumber);

	// Addition Assignment Operator (+=)
	// long method
	// assignmentNumber = assignmentNumber + 2;

	// shorthand method
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);

	assignmentNumber %= 2;
	console.log("Result of modulo assignment operator: " + assignmentNumber);

	// PEMDAS (Order of Operations)

	// Multiple Operators and Parenthesis
	/*
		1. 3*4 = 12
		2. 12/5 = 2.4
		3. 1 + 2 = 3
		4. 3- 2.4 = 0.6 // actual

	*/
	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of MDAS operator: " +mdas);

	// The order of operations can be changed by adding a parenthesis

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of PEMDAS operation: " +pemdas);

	pemdas = (1 + (2 - 3)) * (4 / 5);
	console.log("Result of PEMDAS operation: " +pemdas);

// [SECTION] Increment and Decrement 
	// This operators "add" or "subtract" values by 1 and reassign the value of the variables where the increment/decrement was applied.

	let z = 1;

	// Increment (++)
	// The value of "z" is added by one before storing it in the increment variable.
	let increment = ++z;
	// pre-increment both increment and z variable have value of "2"
	console.log("Result of pre-increment: " +increment);
	console.log("Result of pre-increment for z: " +z);

	// The value of "z" is stored in the "increment" variable before it is increased by one"
	increment = z++;
	console.log("Result of post-increment: " +increment);
	console.log("Result of post-increment for z: " +z);

	// z + z = z
	// z = z + z

	// Decrement(--)
	z = 5;
	let decrement = --z;
	console.log("Result of pre-decrement: "+decrement);
	console.log("Result of pre-decrement for z: " +z);

	decrement = z--;
	console.log("Result of post-decrement: "+decrement);
	console.log("Result of post-decrement for z: " +z);

// [SECTION] Type Coercion
	// automatic or implicit conversion of value from one data type to another.
	// This happens when operations are performed on different data types that would normally possible and yield irregular results.
	// "automatic conversion"

	// 
	let numA = '10';
	let numB = 12;

	// Performing addition operation to a number and string variable will result to a concatenation.

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion); //string

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion); //number

	/*
		- The boolean true is also associated with the value of 1.
		- The boolean false is also associated with the value of 0.
	*/

	let numE = true + 1;
	console.log(numE);

	let numF = false + 1;
	console.log(numF);

// [SECTION] Comparison Operator
	// are used to evaluate and compare the left and right operands.
	// it returns a boolean value true or false

	let juan = 'juan';

	// Equality Operator (==)
	/*
		-Checks whether the operands are equal/have the same content.
		- Attempts to CONVERT AND COMPARE operands of different data types. (type coercion)
	*/
	console.log("Equality Operator; ");
	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true
	console.log(false == 0); //true
	console.log('juan' == "juan"); //true
	console.log("juan" == "Juan"); //false
	console.log(juan == 'juan') //true

	// Inequality Operator (!=)
	/*
		- Checks whether the operands are not equal/have different ata types.
		- Attempts to CONVERT AND COMPARE operands of different data types.
	*/
	console.log("Inequality Operator");
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); //false
	console.log(false != 0); //false
	console.log('juan' != "juan"); //false
	console.log("juan" != "Juan"); //true
	console.log(juan != 'juan'); //false


	// Strict Equality Operator (===)
	/*
		- check whether the operands are equal/have the same content.
		- also COMPARES the data types of 2 values.
	*/

	console.log("Strictly Equality Operator: ")
	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === '1'); //false
	console.log(false === 0); //false
	console.log('juan' === "juan"); //true
	console.log("juan" === "Juan"); //false
	console.log(juan === 'juan'); //true

	// Strict Inequality Operator (!==)
	/*
		- Checks where the operands are not equal/have the different content.
		- also compares the data type of 2 values.
	*/

	console.log("Strict Inequality Operator: ")
	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== '1'); //true
	console.log(false !== 0); //true
	console.log('juan' !== "juan"); //false
	console.log("juan" !== "Juan"); //true
	console.log(juan !== 'juan'); //false

	//[SECTION] Greater Than and Less Than Operator
		// Some comparison operators check whether one value is greater or less than to the other value.
		// Returns a Boolean value.

	let a = 50;
	let b = 65;

	console.log("Greater than and Less than Operator:")
	// GT or Greater than (>)
	let isGreaterThan = a > b; //false
	console.log(isGreaterThan);	

	// LT of Less than (<)
	let isLessThan = a < b; //true
	console.log(isLessThan);

	// GTE or Greater Than or Equal (>=)
	// b = 50; true // changing the value of b to 50 will result to true.
	let isGTorEqual = a >= b; //false
	console.log(isGTorEqual); 

	// LTE or Less Than or Equal (<=)
	let isLTorEqual = a <= b; //true
	console.log(isLTorEqual);

	let numStr = "30";
	console.log(a > numStr); //true - forced coercion to change string to a number.

	let strNum = "twenty";
	console.log(b > strNum); //false - whatever operator is used it will result to false, since the string is not numeric. This is usually result to NaN (Not a Number)	

// [SECTION] Logical Operators
	// to allow for a more specific logical combination of conditions and evaluations.
	// It returns a Boolean value.

	let isLegalAge = true;
	let isRegistered = false;

	// Logical And Operator (&& - Double Ampersand)
	// Returns TRUE if all operands are true.
	let allRequirementsMet = isLegalAge &&
		isRegistered;
	console.log("Result of Logical AND Operator: " + allRequirementsMet); //false

	// true false = false
	// false true = false
	// true true = true
	// false false = false

	// Logical OR Operator (|| - Double Pipe)
	// Returns true if one of the operands is TRUE.
	let someRequirementsMet = isLegalAge || isRegistered; //true
	console.log("Result of Logical OR Operator: " + someRequirementsMet);

	// Logical Not Operator (! - Exclamation Point)
	// Returns the opposite value.
	console.log("Result of Logical NOT Operator: " + !isRegistered); //true
	console.log("Result of logical NOT Operator: " + !isLegalAge); //false